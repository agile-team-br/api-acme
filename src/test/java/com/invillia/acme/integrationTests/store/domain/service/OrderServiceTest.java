package com.invillia.acme.integrationTests.store.domain.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.invillia.acme.InvilliaApplication;
import com.invillia.acme.domain.entity.dtos.OrderDto;
import com.invillia.acme.domain.entity.inputs.AddressInput;
import com.invillia.acme.domain.entity.inputs.ItemInput;
import com.invillia.acme.domain.entity.inputs.OrderInput;
import com.invillia.acme.domain.entity.inputs.StoreInput;
import com.invillia.acme.domain.service.OrderService;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = InvilliaApplication.class, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class OrderServiceTest {
	
	@Autowired
	private OrderService service;
	
	private OrderInput order;
	
	@Before
	public void setUp() {
		order = new OrderInput();
		buildOrderInput();
	}

	
	@Test
	public void deveCriarUmPedido() {
		OrderDto result = new OrderDto();
		result = service.createOrder(order);
		
		assertEquals(2, result.getItens().size());
	}
	
	private void buildOrderInput() {
		AddressInput address = new AddressInput();
		StoreInput store = new StoreInput();
		List<ItemInput> itens = new ArrayList<ItemInput>();
		
		address.setId(new Long(1));
		store.setId(new Long(1));
		
		order.setAddress(address);
		order.setStore(store);
		
		ItemInput i1 = new ItemInput();
		i1.setId(new Long(1));
		i1.setQuantity(22);
		
		ItemInput i2 = new ItemInput();
		i2.setId(new Long(2));
		i2.setQuantity(33);
		
		itens.add(i1);
		itens.add(i2);
		
		order.setItens(itens);
		
	}
	
	
	

}
