package com.invillia.acme.integrationTests.store.domain.service;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.invillia.acme.InvilliaApplication;
import com.invillia.acme.domain.entity.Store;
import com.invillia.acme.domain.entity.inputs.AddressInput;
import com.invillia.acme.domain.entity.inputs.StoreInput;
import com.invillia.acme.domain.service.StoreService;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = InvilliaApplication.class, loader = AnnotationConfigContextLoader.class)
@SpringBootTest
public class StoreServiceTest {
	
	@Autowired
	private StoreService storeService;
	
	private StoreInput  store;
	
	private AddressInput address;
	
	@Before
	public void setUp() {
		
		address = new AddressInput();
		address.setCity("Rio de Janeiro");
		address.setHouseNumber(34);
		address.setState("RJ");
		address.setStreetAddress("Rua do Ouvidor");
		address.setZip("24120222");
		store = new StoreInput("Acme", address);
		
	}
	
	
	@Test
	public void deveCriarUmStore() {
		Store result = new Store();
		result = storeService.createStore(store);
		
		assertEquals(store.getName(), result.getName());
	}
}
