package com.invillia.acme.unitTests.note.domain.entity;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.invillia.acme.domain.entity.Store;


public class StoreTest {



	@Test
	public void shouldNotRaiseViolationWhenTitleAndContentAreFilled() {
		Store note = new Store("Acme");
		assertEquals("Acme", note.getName());
	}
	

}
