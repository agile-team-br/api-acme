package com.invillia.acme.domain.entity.dtos;

import java.util.List;

public class OrderDto {
	
	private Long id;

	private StoreDto store;
	
	private AddressDto address;
	
	private List<ItemDto> itens;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StoreDto getStore() {
		return store;
	}

	public void setStore(StoreDto store) {
		this.store = store;
	}

	public AddressDto getAddress() {
		return address;
	}

	public void setAddress(AddressDto address) {
		this.address = address;
	}

	public List<ItemDto> getItens() {
		return itens;
	}

	public void setItens(List<ItemDto> itens) {
		this.itens = itens;
	}
	
	

	
	
	
	
}
