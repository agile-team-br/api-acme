package com.invillia.acme.domain.entity.inputs;

public class AddressInput {
	
	private Long id;

	private int houseNumber;
	
	private String streetAddress;
	
	private String city;
	
	private String state;
	
	private String zip;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}
	
	public boolean existHouseNumber() {
		return this.houseNumber != 0; 
	}
	
	public boolean existStreetAddress() {
		return this.streetAddress != null;
	}
	
	public boolean existCity() {
		return this.city != null;
	}
	
	public boolean existState() {
		return this.state != null;
	}
	
	public boolean existZip() {
		return this.zip != null;
	}
	
	

}
