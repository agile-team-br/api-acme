package com.invillia.acme.domain.entity.inputs;

public class StoreInput {
	
	private Long id;
	
	private String name;
	
	private AddressInput address;
	
	public StoreInput() {}
	
	public StoreInput(String name, AddressInput address) {
		this.name = name;
		this.address = address;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public AddressInput getAddress() {
		return address;
	}

	public void setAddress(AddressInput address) {
		this.address = address;
	}
	
	public boolean existAddress() {
		return this.address != null;
	}
	
	
	

}
