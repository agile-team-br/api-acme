package com.invillia.acme.domain.entity.inputs;

import java.util.List;

public class OrderInput {
	
	private Long id;

	private StoreInput store;
	
	private AddressInput address;
	
	private List<ItemInput> itens;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public StoreInput getStore() {
		return store;
	}

	public void setStore(StoreInput store) {
		this.store = store;
	}

	public AddressInput getAddress() {
		return address;
	}

	public void setAddress(AddressInput address) {
		this.address = address;
	}

	public List<ItemInput> getItens() {
		return itens;
	}

	public void setItens(List<ItemInput> itens) {
		this.itens = itens;
	}
	
	
}
