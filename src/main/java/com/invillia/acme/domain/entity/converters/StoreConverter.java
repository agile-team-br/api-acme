package com.invillia.acme.domain.entity.converters;

import org.springframework.stereotype.Service;

import com.invillia.acme.domain.entity.Store;
import com.invillia.acme.domain.entity.dtos.StoreDto;

@Service
public class StoreConverter {

	public StoreDto toDto(Store store) {
		StoreDto dto = new StoreDto();

		dto.setId(dto.getId());
		dto.setName(dto.getName());

		return dto;
	}

}
