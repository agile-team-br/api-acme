package com.invillia.acme.domain.entity.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invillia.acme.domain.entity.Address;
import com.invillia.acme.domain.entity.dtos.AddressDto;
import com.invillia.acme.domain.repository.IAddressRepository;

@Service
public class AddressConverter {
	
	
	@Autowired
	private IAddressRepository repository;


	public AddressDto toDto(Address address) {
		AddressDto dto = new AddressDto();
		
		address = repository.findById(address.getId()).get();
		
		dto.setId(address.getId());
		dto.setCity(address.getCity());
		dto.setHouseNumber(address.getHouseNumber());
		dto.setState(address.getState());
		dto.setStreetAddress(address.getStreetAddress());
		dto.setZip(address.getZip());
		
		return dto;
	}
	
	
	

}
