package com.invillia.acme.domain.entity.converters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invillia.acme.domain.entity.Item;
import com.invillia.acme.domain.entity.OrderItem;
import com.invillia.acme.domain.entity.dtos.ItemDto;
import com.invillia.acme.domain.repository.IItemRepository;

@Service
public class ItensConverter {
	
	@Autowired
	private IItemRepository repository;


	public List<ItemDto> toDto(List<OrderItem> itens) {
		List<ItemDto> listDto = new ArrayList<ItemDto>();
		
		itens.forEach(i -> {
			ItemDto dto = new ItemDto();
			Item item = new Item();
			
			item = repository.findById(i.getItem().getId()).get();
			
			dto.setId(i.getItem().getId());
			dto.setDescription(item.getDescription());
			dto.setPrice(item.getPrice());
			dto.setQuantity(i.getQuantity());
			
			listDto.add(dto);
		});
		
		
		return listDto;
	}

}
