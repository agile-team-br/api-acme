package com.invillia.acme.domain.service.datafetcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.invillia.acme.domain.entity.dtos.OrderDto;
import com.invillia.acme.domain.entity.inputs.OrderInput;
import com.invillia.acme.domain.service.OrderService;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class CreateOrderDataFetcher implements DataFetcher<OrderDto> {

	@Autowired
	private OrderService service;

	@Override
	public OrderDto get(DataFetchingEnvironment environment) {
		
		ObjectMapper objectMapper = new ObjectMapper();

		Object rawInput = environment.getArgument("input");
		OrderInput orderInput = objectMapper.convertValue(rawInput, OrderInput.class);

		return service.createOrder(orderInput);
		
	}

}
