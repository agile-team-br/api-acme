package com.invillia.acme.domain.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.invillia.acme.domain.service.datafetcher.CreateOrderDataFetcher;
import com.invillia.acme.domain.service.datafetcher.CreateStoreDataFetcher;
import com.invillia.acme.domain.service.datafetcher.DeleteStoreDataFetcher;
import com.invillia.acme.domain.service.datafetcher.FilterItensDataFetcher;
import com.invillia.acme.domain.service.datafetcher.FilterStoreDataFetcher;
import com.invillia.acme.domain.service.datafetcher.UpdateStoreDataFetcher;

import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

@Service
public class GraphQLStoreService {
	
	@Value("classpath:store.graphqls")
	Resource resource;

	private GraphQL graphQL;
	
	@Autowired
	private CreateStoreDataFetcher createStoreDataFetcher;
	
	@Autowired
	private CreateOrderDataFetcher createOrderDataFetcher;
	
	@Autowired
	private DeleteStoreDataFetcher deleteStoreDataFetcher;
	
	@Autowired
	private UpdateStoreDataFetcher updateStoreDataFetcher;
	
	@Autowired
	private FilterStoreDataFetcher filterStoreDataFetcher;
	
	@Autowired
	private FilterItensDataFetcher filterItensDataFetcher;
	
	@PostConstruct
	private void loadSchema() throws IOException {
		

		// get the schema
		InputStream schemaFile = resource.getInputStream();

		InputStreamReader isReader = new InputStreamReader(schemaFile);
		
	    //Creating a BufferedReader object
	      BufferedReader reader = new BufferedReader(isReader);
	      StringBuffer sb = new StringBuffer();
	      String str;
	      while((str = reader.readLine())!= null){
	         sb.append(str);
	      }

		// parse schema
		TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(sb.toString());
		RuntimeWiring wiring = buildRuntimeWiring();
		GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(typeRegistry, wiring);
		graphQL = GraphQL.newGraphQL(schema).build();
	}
	
	private RuntimeWiring buildRuntimeWiring() {
		return RuntimeWiring.newRuntimeWiring()
				.type("Mutation",
						typeWiring -> typeWiring
						.dataFetcher("createStore", createStoreDataFetcher)
						.dataFetcher("deleteStore", deleteStoreDataFetcher)
						.dataFetcher("updateStore", updateStoreDataFetcher)
						.dataFetcher("createOrder", createOrderDataFetcher)
						)
				.type("Query",
						typeWiring -> typeWiring
						.dataFetcher("getAllStores", filterStoreDataFetcher)
						.dataFetcher("getAllItensByStore", filterItensDataFetcher)
						).build();
	}
	
	public GraphQL getGraphQL() {
		return graphQL;
	}
	


}
