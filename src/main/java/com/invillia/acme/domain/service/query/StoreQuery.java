package com.invillia.acme.domain.service.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.invillia.acme.domain.entity.Item;
import com.invillia.acme.domain.entity.Store;
import com.invillia.acme.domain.entity.inputs.StoreInput;
import com.invillia.acme.domain.service.StoreService;

@Component
public class StoreQuery implements GraphQLQueryResolver{
	
	
	@Autowired
	private StoreService service;
	
	public List<Store> getAllStores(StoreInput input) {
		return service.getAllStores(input);
	}
	
}
