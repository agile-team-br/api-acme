package com.invillia.acme.domain.service.mutation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.invillia.acme.domain.entity.Store;
import com.invillia.acme.domain.entity.dtos.OrderDto;
import com.invillia.acme.domain.entity.inputs.OrderInput;
import com.invillia.acme.domain.entity.inputs.StoreInput;
import com.invillia.acme.domain.service.OrderService;
import com.invillia.acme.domain.service.StoreService;

@Component
public class StoreMutation implements GraphQLMutationResolver{
	
	@Autowired
	private StoreService service;
	
	@Autowired
	private OrderService orderService;
	
	public Store createStore(StoreInput input) {
		return service.createStore(input);
	}
	
	public boolean deleteStore(String id) {
		return service.deleteStore(Long.parseLong(id));
	}
	
	public Store updateStore(StoreInput input) {
		return service.updateStore(input);
	}
	
	public OrderDto createOrder(OrderInput input) {
		return orderService.createOrder(input);
	}
	

}
