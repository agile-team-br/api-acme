package com.invillia.acme.domain.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invillia.acme.domain.entity.Address;
import com.invillia.acme.domain.entity.Item;
import com.invillia.acme.domain.entity.OrderAcme;
import com.invillia.acme.domain.entity.OrderItem;
import com.invillia.acme.domain.entity.Store;
import com.invillia.acme.domain.entity.converters.AddressConverter;
import com.invillia.acme.domain.entity.converters.ItensConverter;
import com.invillia.acme.domain.entity.converters.StoreConverter;
import com.invillia.acme.domain.entity.dtos.OrderDto;
import com.invillia.acme.domain.entity.inputs.ItemInput;
import com.invillia.acme.domain.entity.inputs.OrderInput;
import com.invillia.acme.domain.repository.IOrderRepository;

@Service
public class OrderService {

	@Autowired
	private IOrderRepository repository;

	@Autowired
	private AddressConverter addressConverter;

	@Autowired
	private StoreConverter storeConverter;

	@Autowired
	private ItensConverter itensConverter;

	@Transactional
	public OrderDto createOrder(OrderInput input) {

		OrderAcme order = new OrderAcme();
		Store store = new Store();
		Address address = new Address();
		store.setId(input.getStore().getId());
		address.setId(input.getAddress().getId());

		order.setAddress(address);
		order.setStore(store);
		order.setItens(saveItens(input.getItens(), order));

		return convertToOrderDto(repository.save(order));
	}

	private OrderDto convertToOrderDto(OrderAcme result) {
		OrderDto dto = new OrderDto();

		dto.setId(result.getId());
		dto.setAddress(addressConverter.toDto(result.getAddress()));
		dto.setStore(storeConverter.toDto(result.getStore()));
		dto.setItens(itensConverter.toDto(result.getItens()));

		return dto;
	}

	private List<OrderItem> saveItens(List<ItemInput> inputItens, OrderAcme order) {
		List<OrderItem> itens = new ArrayList<OrderItem>();

		inputItens.forEach(i -> {
			OrderItem orderItem = new OrderItem();
			Item item = new Item();

			item.setId(i.getId());
			orderItem.setItem(item);
			orderItem.setOrderAcme(order);
			orderItem.setQuantity(i.getQuantity());

			itens.add(orderItem);
		});

		return itens;
	}

}
