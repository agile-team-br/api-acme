package com.invillia.acme.domain.service.datafetcher;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.invillia.acme.domain.entity.Store;
import com.invillia.acme.domain.entity.inputs.StoreInput;
import com.invillia.acme.domain.service.StoreService;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class FilterStoreDataFetcher implements DataFetcher<List<Store>> {

	@Autowired
	private StoreService service;

	@Override
	public List<Store> get(DataFetchingEnvironment environment) {

		ObjectMapper objectMapper = new ObjectMapper();

		Object rawInput = environment.getArgument("input");
		StoreInput input = objectMapper.convertValue(rawInput, StoreInput.class);

		return service.getAllStores(input);
	}

}
