package com.invillia.acme.domain.service.datafetcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.invillia.acme.domain.service.StoreService;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class DeleteStoreDataFetcher implements DataFetcher<Boolean>{
	
	@Autowired
	private StoreService service;
	
	@Override
	public Boolean get(DataFetchingEnvironment environment) {
		
		String id = environment.getArgument("id");
		
		return service.deleteStore(Long.parseLong(id));
	}

}
