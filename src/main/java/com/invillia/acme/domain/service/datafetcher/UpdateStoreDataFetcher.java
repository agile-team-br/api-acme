package com.invillia.acme.domain.service.datafetcher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.invillia.acme.domain.entity.Store;
import com.invillia.acme.domain.entity.inputs.StoreInput;
import com.invillia.acme.domain.service.StoreService;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

@Component
public class UpdateStoreDataFetcher implements DataFetcher<Store> {

	@Autowired
	private StoreService service;

	@Override
	public Store get(DataFetchingEnvironment environment) {
		
		ObjectMapper objectMapper = new ObjectMapper();

		Object rawInput = environment.getArgument("input");
		StoreInput store = objectMapper.convertValue(rawInput, StoreInput.class);

		return service.updateStore(store);
	}

}
