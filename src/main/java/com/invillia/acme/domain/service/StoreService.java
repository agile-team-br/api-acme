package com.invillia.acme.domain.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invillia.acme.domain.entity.Address;
import com.invillia.acme.domain.entity.Item;
import com.invillia.acme.domain.entity.Store;
import com.invillia.acme.domain.entity.inputs.AddressInput;
import com.invillia.acme.domain.entity.inputs.StoreInput;
import com.invillia.acme.domain.repository.IAddressRepository;
import com.invillia.acme.domain.repository.IItemRepository;
import com.invillia.acme.domain.repository.IStoreRepository;

@Service
public class StoreService {

	@Autowired
	private IStoreRepository repository;
	
	@Autowired
	private IAddressRepository addressRepository;
	
	@Autowired
	private IItemRepository itemRepository;

	@Transactional
	public List<Store> getPersons() {

		return (List<Store>) repository.findAll();

	}

	@Transactional
	public Store createStore(StoreInput input) {

		Store store = new Store();
		store.setName(input.getName());
		
		if(input.existAddress()) {
			Address address = new Address(input.getAddress());
			store.setAddress(addressRepository.save(address));
		}
		
		
		return repository.save(store);

	}

	public Boolean deleteStore(Long id) {
		repository.deleteById(id);
		return true;
	}

	public Store updateStore(StoreInput input) {
		Optional<Store> store = repository.findById(input.getId());
		input.getAddress().setId(store.get().getAddress().getId());
		
		store.ifPresent(s -> {
			s.setName(input.getName());
			s.setAddress(updateAddress(input.getAddress()));
		});
		return this.repository.save(store.get());
	}

	private Address updateAddress(AddressInput input) {
		Address address = addressRepository.getOne(input.getId());
		
		if(input.existHouseNumber()) address.setHouseNumber(input.getHouseNumber());
		if(input.existStreetAddress()) address.setStreetAddress(input.getStreetAddress());
		if(input.existCity()) address.setCity(input.getCity());
		if(input.existState()) address.setState(input.getState());
		if(input.existZip()) address.setZip(input.getZip());
		
		return addressRepository.save(address);
	}

	public List<Store> getAllStores(StoreInput input) {
		return (List<Store>) repository.findAll();
	}
	
	public List<Item> getAllItensByStore(StoreInput input) {
		Optional<Store> store = repository.findById(input.getId());
		
		return this.itemRepository.findByStore(store.get());
	}
	
	public Store getByIdStore(Long id) {
		return this.repository.findById(id).get();
	}

}
