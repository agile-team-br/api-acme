package com.invillia.acme.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.invillia.acme.domain.entity.Item;
import com.invillia.acme.domain.entity.Store;


@Repository
public interface IItemRepository extends JpaRepository<Item, Long>{

	public List<Item> findByStore(Store store);
	
}
