package com.invillia.acme.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.invillia.acme.domain.entity.Store;

@Repository
public interface IStoreRepository extends JpaRepository<Store, Long>{

}
