package com.invillia.acme.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.invillia.acme.domain.entity.Address;

@Repository
public interface IAddressRepository  extends JpaRepository<Address, Long>{

}
