package com.invillia.acme.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.invillia.acme.domain.entity.OrderAcme;


@Repository
public interface IOrderRepository extends JpaRepository<OrderAcme, Long>{
	

}
