package com.invillia.acme.api.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.invillia.acme.domain.service.GraphQLStoreService;

import graphql.ExecutionResult;

@RequestMapping("/api-acme/stores")
@RestController
public class StoreResource {
	
	@Autowired
	GraphQLStoreService graphQLService;

	@PostMapping
	public ResponseEntity<Object> persons(@RequestBody String query) {
		ExecutionResult execute = graphQLService.getGraphQL().execute(query);

		return new ResponseEntity<>(execute, HttpStatus.OK);
	}

}
