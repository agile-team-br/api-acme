
# API ACME / INVILLIA 
_Prerequisitos:_
* docker version 1.10.3
* JDK8
* Maven

## Features

- `Springboot` 
- `Swagger` 
- `GraphQL` 
- `H2` 
- `Liquibase` 

## Instalação

1. Seguindo a ordem de instalação você precisará do docker instalado em sua máquina para gerar a imagem da aplicação com o seguinte comando na raiz do projeto:

   `mvn clean package docker:build -Dmaven.test.skip=true`
   
2. Uma vez a imagem criada você poderá rodar o container da aplicação com o seguinte comando:

   `docker run -p 8080:8080 api_acme_web`

3. Uma vez o container levantado você vai poder acessar e testar pelas urls:

- `Entrypoint Rest >> http://localhost:8080/api-acme/stores` 
- `Entrypoint API Graphiql >> http://localhost:8080/graphiql` 
- `Entrypoint Swagger >> http://localhost:8080/swagger-ui.html#` 
 

## Testando a API via Graphiql ou Postman:

## GraphQL 


### GET / Itens

> Ao Inicializar a aplicação é dado carga automaticamente pelo Liquibase de alguns itens para exemplo, pode executar a consulta abaixo para recuperar os pedidos inclusos.
```
{
  getAllItensByStore(input: { id: 1 }) 
 {
    id
    description
    price
    store {
      id
      name
    }
  }
}

```



### CREATE uma Store

> Criando uma Store com seu Address, observe que o GraphQL atende a quantidade de campos que deseja como retorno.
```
mutation {
  createStore(input: {name: "ACME - INVILLIA", address: {  houseNumber: 23, streetAddress: "Rua do Ouvidor", city: "Rio de Janeiro", state: "RJ", zip: "24120202" } }) 
  { 
    id
    name
    address {
      id
      houseNumber
      streetAddress
      city
      state
      zip
    }
  }
}
```

### GET / Filtro

> Esse entrypoint não só faz a consulta por uma store, mas filtra  por outros campos.  Pelo tempo só fiz consulta por id e name.  Mas a ideia é que tenha um único entrypoint para qualquer tipo de consulta.
```
{
  getAllStores(input: {}) 
 { 
    id
    name
    address {
      id
      houseNumber
      streetAddress
      city
      state
      zip
    }
  }
}

```

### UPDATE

> Atualizando uma Store, observe que o GraphQL atende a quantidade de campos que deseja como retorno, é possivel também alterar seu endereço.
```
mutation {
  updateStore(input: {id: 1, name: "Mudou",  address: {  houseNumber: 42, streetAddress: "Pres. Vargas" } }) 
  { 
    id
    name
    address {
      id
      houseNumber
      streetAddress
      city
      state
      zip
    }
  }
}
```

### DELETE

> Deletando uma Store,  retornará um boolean e status 200.
```
mutation {
	deleteStore(id: 1)
}
```

### CREATE ORDER

> Não deu tempo para fazer o get order by id, mas na criação do order pode-se ver o retorno com o pedido salvo e seus itens, poderia evoluir o order com campos transientes como total de itens, soma dos valores dos itens dando o total da fatura entre outras coisas.

```
mutation {
  createOrder(input: {store: {  id: 1 }, address: { id: 1 }
  itens: [
    { id: 1, quantity: 22},
    { id: 2, quantity: 33}
  ]
  
  }) 
  { 
    id
    store {
     id
    }
    address {
      id
      houseNumber
      streetAddress
      city
      state
      zip
    }
    itens {
      id
      description
      price
      quantity
    }
  }
}
```










